package com.maxim.commodityturnover;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.maxim.commodityturnover.model.Product;

import java.util.List;

/**
 * Created by Максим on 20.05.2017.
 */

public class AdapterProduct extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<Product> products;

    AdapterProduct(Context context, List<Product> product) {
        products = product;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.item_view, parent, false);
        }

        Product p = getProduct(position);

        ((TextView) view.findViewById(R.id.textView_name_product)).setText(p.getNameProduct());
        ((TextView) view.findViewById(R.id.textView_count_product)).setText(String.valueOf(p.getCountProducts()));
        return view;
    }

    private Product getProduct(int position) {
        return ((Product) getItem(position));
    }
}
